provider "linode" {
    token = var.linodeapikey 
}

data "linode_user" "kubernetes" {
  username = var.linode_username 
}

resource "linode_instance" "k3scontrollers" {
    label = "k3scontroller1"
    image = "linode/arch"
    group = "Terraform"
    region = "us-east"
    type =  "g6-standard-1"
    private_ip = true
    authorized_keys = ["${chomp(file(var.ssh_key_pub))}"]
    authorized_users = ["${data.linode_user.kubernetes.username}"]
    root_pass = var.archrootpass
    provisioner "remote-exec" {
        inline = [
            "hostnamectl set-hostname ${self.label}",
            "sed 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' -i /etc/sudoers",
            "useradd -m -g users -G wheel -s /bin/bash ${var.kubernetes_user_name}",
            "echo '${var.kubernetes_user_name}:${var.kubernetes_user_password}' | chpasswd",
            "ip addr add ${self.private_ip_address}/17 dev eth0",
            "pacman -Sy vim-minimal screen curl wget base-devel --noconfirm"
        ]
        connection {
            type = "ssh"
            user = "root"
            private_key = chomp(file(var.ssh_key))
            # password = var.archrootpass
            host = self.ip_address
            agent = false
        }
    }
}

resource "linode_instance" "k3sworkers" {
    count = 2
    label = "k3sworker${count.index}"
    image = "linode/arch"
    group = "Terraform"
    region = "us-east"
    type =  "g6-standard-2"
    private_ip = true
    authorized_keys = ["${chomp(file(var.ssh_key_pub))}"]
    authorized_users = ["${data.linode_user.kubernetes.username}"]
    root_pass = var.archrootpass
    provisioner "remote-exec" {
        inline = [
            "hostnamectl set-hostname ${self.label}",
            "sed 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' -i /etc/sudoers",
            "useradd -m -g users -G wheel -s /bin/bash ${var.kubernetes_user_name}",
            "echo '${var.kubernetes_user_name}:${var.kubernetes_user_password}' | chpasswd",
            "ip addr add ${self.private_ip_address}/17 dev eth0",
            "pacman -Sy vim-minimal screen curl wget base-devel --noconfirm"
        ]
        connection {
            type = "ssh"
            user = "root"
            private_key = chomp(file(var.ssh_key))
            # password = var.archrootpass
            host = self.ip_address
            agent = false
        }
    }
}
