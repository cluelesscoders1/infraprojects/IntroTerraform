provider "linode" {
    token = var.linodeapikey 
}

data "linode_user" "kubernetes" {
  username = var.linode_username 
}

resource "linode_instance" "atest" {
    label = "archdemo1"
    image = "linode/arch"
    group = "Terraform"
    region = "us-east"
    type =  "g6-nanode-1"
    authorized_keys = ["${chomp(file(var.ssh_key_pub))}"]
    authorized_users = ["${data.linode_user.kubernetes.username}"]
    root_pass = var.archrootpass
    provisioner "remote-exec" {
        inline = [
            "hostnamectl set-hostname ${self.label}",
            "sed 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' -i /etc/sudoers",
            "useradd -m -g users -G wheel -s /bin/bash ${var.kubernetes_user_name}",
        ]
        connection {
            type = "ssh"
            user = "root"
            # private_key = chomp(file(var.ssh_key))
            password = var.archrootpass
            host = self.ip_address
            agent = false
        }
    }
}

output "instance_ip_addr"{
    description = "Instance IP Address"
    value       = linode_instance.atest.ip_address
}
