provider "linode" {
    token = var.linodeapikey 
}

data "linode_user" "kubernetes" {
  username = var.linode_username 
}

resource "linode_instance" "atest" {
    label = "archdemo1"
    image = "linode/arch"
    region = "us-east"
    type =  "g6-nanode-1"
}

output "instance_ip_addr"{
    description = "Instance IP Address"
    value       = linode_instance.atest.ip_address
}
