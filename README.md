[[_TOC_]]

# Terraform Example

Example Terraform example for Arch Linux and Linode for ease of understanding and use. 

# Variables

Example variables file to be modified: 
```terraform
variable "linodeapikey" {
  default = "some_long_linode_apikey"
}

variable "archrootpass" {
  default = "the_most_secure_root_password_random_i_swear"
}

variable "ssh_key_pub" {
  description = "SSH Public Key"
  default = "~/.ssh/linodiest.pub"
}

variable "ssh_key" {
  description = "SSH Private Key"
  default = "~/.ssh/linodiest"
}

variable "linode_username" {
  description = "Username to be used as noted in the Linode profile"
  default = "linodesBestKubernetesUser"
}

variable "kubernetes_user_name" {
  description = "Username used for the kubernetes user"
  default = "kuber"
}

variable "kubernetes_user_password"{
  description = "Password for the kubernetes user"
  default = "MostS3cureP@ssword"
}
```

# Files
Files for playing with, importing, or working with a loop structure for a cluster of systems are included the backups folder to copied to the root directory for testing/exploration.

# Example use
Once the variables file is created and the file of choice is copied from backups, the following commands can be run.

1. `terraform init`
1. `terraform plan`
1. `terraform apply`
1. `terraform destroy`
